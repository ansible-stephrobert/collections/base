"""Role testing files using testinfra."""


def test_root_user(host):
    user = host.user("root")
    assert user.exists
    assert user.shell == "/bin/bash"
    assert user.home == f"/{user.name}"
    assert user.group == "root"


def test_root_profile_file(host):
    user_name = "root"
    file_name = f"/{user_name}/.profile"
    file = host.file(file_name)
    assert file.exists
    assert file.is_file
    assert file.user == "root"
    assert file.group == "root"
    assert file.mode == 0o644
    assert file.contains("\nreadonly HISTFILE\n")
    assert file.contains("\nexport SHELL=/bin/bash\n")
    assert file.contains("\nexport LANG=fr_FR.UTF8\n")


def test_admuser_user(host):
    user = host.user("admuser")
    assert user.exists
    assert user.shell == "/bin/bash"
    assert user.home == f"/home/{user.name}"
    assert user.group == "admuser"
    assert "adm" in user.groups


def test_admuser_profile_file(host):
    user_name = "admuser"
    file_name = f"/home/{user_name}/.profile"
    file = host.file(file_name)
    assert file.exists
    assert file.is_file
    assert file.user == "root"
    assert file.group == "root"
    assert file.mode == 0o644
    assert file.contains("\nreadonly HISTFILE\n")
    assert file.contains("\nexport SHELL=/bin/bash\n")


def test_installed_packages(host):
    assert host.package("bash").is_installed
    assert host.package("bash-completion").is_installed
    if str(host.system_info.distribution).lower() in ("centos", "redhat", "amzn"):
        assert host.package("vim-enhanced").is_installed
    else:
        assert host.package("vim").is_installed
    assert host.package("e2fsprogs").is_installed


def test_ssh_admuser(host):
    file_name = "/home/admuser/.ssh"
    file = host.file(file_name)
    assert file.exists
    assert file.is_directory
    assert file.user == "admuser"
    assert file.group == "admuser"
    assert file.mode == 0o700
